.. Python ListenBrainz Module documentation master file, created by
   sphinx-quickstart on Mon Mar 12 14:37:32 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../../README.rst

Installation
=============

**Latest stable release** from `PyPi <https://pypi.org/project/python-listenbrainz/>`_:

.. code:: bash

   pip install python-listenbrainz

**Latest development version** using pip + git:

.. code:: bash

    pip install git+https://gitlab.com/kaliko/python-listenbrainz.git


.. _requests: http://docs.python-requests.org/en/master/


Build documentation
--------------------

.. code:: bash

    # Get the source
    git clone https://gitlab.com/kaliko/python-listenbrainz.git && cd python-listenbrainz
    # Installs sphinx if needed
    python3 -m venv venv && . ./venv/bin/activate
    pip install sphinx
    # And build
    python3 setup.py build_sphinx
    # Or call sphinx
    sphinx-build -d ./doc/build/doctrees doc/source -b html ./doc/build/html


Contents
=========

.. toctree::
   :maxdepth: 2

   doc.rst
   dev.rst
   contribute.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. vim: spell spelllang=en
