In depth documentation
=======================

Client
------

.. automodule:: listenbrainz.core
    :members:
    :undoc-members:
    :private-members:

Exceptions
-----------

.. automodule:: listenbrainz.exceptions
    :members:


Utilities objects and functions
-------------------------------

.. automodule:: listenbrainz.lib
    :members:
    :undoc-members:
    :private-members:


Global Vars
-----------


:py:data:`listenbrainz.ADDITIONAL_INFO_ITEMS`
    Optionnal "additional_info" elements allowed for listens

:py:data:`listenbrainz.ROOT_URL`
    Listenbrainz server root url, defaults to current listenbrainz.org server
    
    cf. https://listenbrainz.readthedocs.io/en/latest/dev/api.html

.. vim: spell spelllang=en
