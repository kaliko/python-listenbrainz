Contributing
=============

Use git, `learn if needed`_.

Git Workflow
-------------
* ``master`` branch holds latest stable|release code
* ``dev`` branch holds current development code
* Work on a dedicated branch starting off ``dev``
* I like fast forward merges
* Advertise your work (cf. Note)

.. NOTE::
    **Git merge Workflow** |br|
    I currently don't care about a specific workflow concerning changes submission. |br|
    gitlab merge request, gh pull request, plain email pointing out a repo/ref. All we need is a public git repo and a ref to fetch from.

Coding
-------

* follow pep8
* write unittest
* actually test your code (unit and functional testing)


.. _`learn if needed`: https://git-scm.com/book/

.. |br| raw:: html

    <br />

.. vim: spell spelllang=en
