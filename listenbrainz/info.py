# coding: utf-8
# Copyright (C) 2018  Kaliko Jack <kaliko@azylum.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Info about module"""

__author__ = 'kaliko jack'
__email__ = 'kaliko@azylum.org'
# Tries to follow Semantic Versioning Specification
# https://semver.org/
__version__ = '0.5.0'
__url__ = 'https://gitlab.com/kaliko/python-listenbrainz'


# VIM MODLINE
# vim: ai ts=4 sw=4 sts=4 expandtab fileencoding=utf8
