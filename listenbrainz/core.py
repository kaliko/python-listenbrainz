# coding: utf-8
# Copyright (C) 2018  Kaliko Jack <kaliko@azylum.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# https://listenbrainz.readthedocs.io/

import collections
import logging
import os

from time import sleep

from requests import Request, Session
from requests.exceptions import RequestException

from . import ROOT_URL, API_VERSION
from .exceptions import ListenBrainzException, ListenBrainzHttpException
from .lib import Listen, auth_needed


TIMEOUT = 5

if 'DEBUG' in os.environ:
    logging.basicConfig(level=logging.DEBUG)

class Client:
    """
    ListenBrainz http client:

    :param str user: user name
    :param str token: user token
    :param str root_url: server url
    :param str api_version: API version
    """

    #: Class attribute tracking rate limits as found in the last HTTP request.
    #:
    #: cf. API documentation for more https://listenbrainz.org/api-docs
    RateLimit = {
        "X-RateLimit-Limit": 42,
        "X-RateLimit-Remaining": 42,
        "X-RateLimit-Reset-In": 42,
        "X-RateLimit-Reset": 42,
        }


    def __init__(self, user, token=None, root_url=ROOT_URL, api_version=API_VERSION):
        self.url = '{}/{}'.format(root_url, api_version)
        self.user = {'name': user, 'token': token}
        self.cli = None
        self.log = logging.getLogger(__name__)
        self._last_req = None

    def _set_rate_limit(self, headers):
        """Import and convert X-Rate headers into class attribute"""
        for key, val in headers.items():
            if not key.startswith('X-Rate'):
                continue
            if val.isdigit():
                val = int(val)
            else:
                val = None
            self.__class__.RateLimit.update({key:int(val)})
        msg = 'Rate limit: %(X-RateLimit-Remaining)s/%(X-RateLimit-Limit)s reset' \
              ' in %(X-RateLimit-Reset-In)ss'
        self.log.debug(msg, self.__class__.RateLimit)

    def _ratelimit_throttle(self):
        """Ratelimit throttling

        Waits for :py:obj:`listenbrainz.core.Client._rlimit_reset` seconds if
        :py:obj:`listenbrainz.core.Client._rlimit` < 1.
        """
        if self._rlimit < 1:
            self.log.debug("Rate limit reached!")
            if self._rlimit_reset > 0:
                self.log.info("Sleeping for %s, rate limit reached!", self._rlimit_reset)
                sleep(self._rlimit_reset)

    @property
    def _rlimit(self):
        """Exposes class attribut ``RateLimit['X-RateLimit-Remaining']``.\n
        Defaults to ``42`` when no request have been sent yet.

        :returns: Number of requests remaining in current time window
        :rtype: int
        """
        limit = self.__class__.RateLimit.get('X-RateLimit-Remaining')
        return limit

    @property
    def _rlimit_reset(self):
        """Exposes class attribut ``RateLimit['X-RateLimit-Reset-In']``.\n
        Defaults to ``42`` when no request have been sent yet.

        :returns: Number of seconds when current time window expires, cf. :py:obj:`listenbrainz.core.Client._rlimit`
        :rtype: int
        """
        limit = self.__class__.RateLimit.get('X-RateLimit-Reset-In')
        return limit

    def _translate_listens(self, js):
        """Converts listens items in json payload to Listen objects

        :param dict js: JSON payload as returned by user/{name}/listens
        :returns: payload as :py:obj:`dict` but with listens as a list of :py:obj:`listenbrainz.lib.Listen` objects
        :rtype: dict

            >>> js = { 'payload': { 'listens': [{…}, {…},…]}}
        """
        listens = []
        for listen in js.get('payload').get('listens'):
            ts = listen.get('listened_at')
            add_info = listen.get('track_metadata').get('additional_info')
            tname = listen.get('track_metadata').get('track_name')
            aname = listen.get('track_metadata').get('artist_name')
            listens.append(Listen(tname, aname, ts, **add_info))
        js.get('payload')['listens'] = listens
        return js

    def _request(self, ress, params=None, payload=None, auth=False, verb='GET'):
        """
            Abstract request, private method.

            :param str ress: http ressource to consume
            :param dict payload: payload
            :type payload: dict or None
            :param dict params: URL parameters parameters (GET verb) or form-encoded body data (POST verb), please refer to ``requests.Request``
            :type params: dict or None
            :param bool auth: send Authorization token
            :param str verb: HTTP method (GET or POST)
            :returns: HTTP answer
            :rtype: ``requests.Response``

            .. note::
              Parameter ``params`` is:

                * URL parameters when the request is set to GET.
                * form-encoded body data when the request is set to GET.

              Please refer to http://docs.python-requests.org/en/master/api/#requests.request
        """
        self._ratelimit_throttle()
        if verb == 'GET':
            kwargs = {'params': params}
        if verb == 'POST':
            kwargs = {'data': params}
        headers = {}
        if auth:
            self.log.debug('Authorization token added')
            headers = {'Authorization': 'Token {}'.format(self.user.get('token'))}
        url = '{}/{}'.format(self.url, ress)
        req = Request(verb, url, headers=headers, json=payload, **kwargs).prepare()
        sess = Session()
        self.log.debug(req.url)
        try:
            resp = sess.send(req, timeout=TIMEOUT,)
            self._last_req = resp
        except RequestException as err:
            raise ListenBrainzHttpException(err)
        self._set_rate_limit(resp.headers)
        if resp.status_code != 200:
            error = {'status_code': resp.status_code,
                     'reason': resp.reason}
            try:
                error['reason'] = resp.json().get('error')
            except ValueError:
                pass
            raise ListenBrainzHttpException('Server returned {status_code}: {reason}'.format(**error))
        content_type = resp.headers['content-type']
        if content_type != 'application/json':
            raise ListenBrainzHttpException('Unhandled content type: {}'.format(content_type))
        self.log.debug('Server answered: %s', resp.json())
        return resp

    def listens(self, min_ts=None, max_ts=None, count=None):
        """Get listens for user

            :param int min_ts: listens with listened_at less than (but not including) this value will be returned.
            :param int max_ts: listens with listened_at greater than (but not including) this value will be returned.
            :param int count: number of listens to return.
            :returns: returns a list of listens:  { "count": 25, "listens": [{…}, {…}, …}], "user_id": "username" }
            :rtype: dict

            .. note::
              ``min_ts``/``max_ts`` as UNIX epoch timestamps in UTC.

            .. note::
              Returns a dictionary :

                  >>> client = listenbrainz.Client('username',)
                  >>> print(client.listens(count=4))
                  {'payload': {'count': 4,
                    'listens': [
                       Listen(title="The Huntsmen", artist="Mantar", listened_at="1521393075),
                       Listen(title="The Huntsmen", artist="Mantar", listened_at="1520950368),
                       Listen(title="XXII. Q. Culture A. Resist", artist="Masakari", listened_at="1520933583),
                       Listen(title="Elogia de la sombra", artist="Master Musicians of Bukkake", listened_at="1520932583)],
                    'user_id': 'username'}}
        """
        params = {'count': count}
        if min_ts:
            params.update({'min_ts': min_ts})
        if max_ts:
            params.update({'max_ts': max_ts})
        ressource = 'user/{name}/listens'.format(**self.user)
        resp = self._request(ressource, params=params)
        return self._translate_listens(resp.json())

    @auth_needed
    def submit_playing_now(self, listen):
        """Submit listens: type:playing_now

        Notification expire after 5 minutes ?

        cf. https://tickets.metabrainz.org/browse/LB-119

        :param listenbrainz.lib.Listen listen: :py:obj:`listenbrainz.lib.Listen` object
        """
        if not isinstance(listen, Listen):
            raise ListenBrainzException('Not an instance of Listen: {}'.format(listen))
        # Remove timestamp for playing_now
        payload = {"listen_type": "playing_now", "payload": [listen.payload(with_ts=False)]}
        ressource = 'submit-listens'
        self.log.debug("submit payload: %s", payload)
        resp = self._request(ressource, payload=payload, verb='POST', auth=True)
        return resp

    @auth_needed
    def submit_single(self, listen):
        """Submit listens: type:single

        :param listenbrainz.lib.Listen listen: :py:obj:`listenbrainz.lib.Listen` object
        """
        if not isinstance(listen, Listen):
            raise ListenBrainzException('Not an instance of Listen: {}'.format(listen))
        payload = {"listen_type": "single", "payload": [listen.payload()]}
        ressource = 'submit-listens'
        self.log.debug("submit payload: %s", payload)
        resp = self._request(ressource, payload=payload, verb='POST', auth=True)
        return resp

    @auth_needed
    def submit_import(self, listens):
        """Submit listens: type:import (Submit previously saved listens)

        :param list list of listens: iterable of :py:obj:`listenbrainz.lib.Listen` objects
        """
        if isinstance(listens, collections.Iterable):
            for trk in listens:
                if not isinstance(trk, Listen):
                    raise ListenBrainzException('Not an instance of Listen: {}'.format(trk))
        payload = {"listen_type": "import", "payload": [trk.json for trk in listens]}
        ressource = 'submit-listens'
        resp = self._request(ressource, payload=payload, verb='POST', auth=True)

    def latest_import(self, timestamp=None):
        """Update/Get the timestamp of the newest listen submitted

        When `timestamp` is set, the client will `POST` instead of `GET`

        :param int timestamp: UNIX epoch timestamps in UTC.
        """
        ressource = 'latest-import'
        if timestamp:
            if not self.user.get('token'):
                raise ListenBrainzException('Missing token, Authorization needed for this call!')
            param = None
            payload = {'ts': timestamp}
            auth = True
            verb = 'POST'
        else:
            param = {'user_name': self.user.get('name')}
            payload = None
            auth = False
            verb = 'GET'
        resp = self._request(ressource, param, payload, auth, verb)
        return resp.json()


# VIM MODLINE
# vim: ai ts=4 sw=4 sts=4 expandtab fileencoding=utf8
